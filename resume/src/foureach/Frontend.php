<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\foureach;
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use App\foureach\Required;


/**
 * Description of Frontend
 *
 * @author sonjoy
 */
class Frontend {
    //put your code here
    public function __construct() {}
    
    public function all_users_info(){
        $data = array();
        $query = "SELECT * FROM `users`";
        
        $result = mysql_query($query);
        while ($row = mysql_fetch_object($result)) {
            $data[] = $row;
        }
//        Required::debug($data);
        return $data;
    }
    public function logout(){
//        unset($_SESSION['id']);
//        unset($_SESSION['username']);
        session_destroy();
        Required::success_message("You are successfully Logout!!");
        Required::redirect1();
    }
}
