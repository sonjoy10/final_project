<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
include '../vendor/autoload.php';
//require $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARETOR.'resume'.DIRECTORY_SEPARETOR.'vendor'.DIRECTORY_SEPARETOR.'autoload.php';

use App\foureach\Frontend;
use App\foureach\Required;

$resume = new Frontend();
$resumes = $resume->all_users_info();

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */

require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'resume'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'phpoffice'.DIRECTORY_SEPARATOR.'phpexcel'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php';



// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Sonjoy")
							 ->setLastModifiedBy("Sonjoy")
							 ->setTitle("Office 2010 XLSX Test Document")
							 ->setSubject("Office 2010 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2010 openxml php")
							 ->setCategory("Test result file");


$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'User Name')
            ->setCellValue('B1', 'Email')
            ->setCellValue('C1', 'Password');
$counter = 2;


//Requered::debug($phonebooks);
foreach($resumes as $resume){
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter, $resume->user_name)
            ->setCellValue('B'.$counter, $resume->email)
            ->setCellValue('C'.$counter, $resume->password);

    $counter++;
}

        
      
         

//// Miscellaneous glyphs, UTF-8
//$objPHPExcel->setActiveSheetIndex(0)
//            ->setCellValue('A4', 'Miscellaneous glyphs')
//            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Resume|| users');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="4eachresume.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2010');
$objWriter->save('php://output');
exit;
