<div class="container">
    <div class="col-md-5 col-md-offset-4 login">
        <h1>LOGIN</h1>
        <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
//        session_start();
        use App\foureach\Required;

$success = Required::success_message();
//        print_r($_SESSION);

        if (isset($success)) {
            ?>
            <div class = "alert alert-success">
                <button type = "button" class = "close" data-dismiss = "alert">
                    <i class = " fa fa-times"></i>
                </button>
                <p>
                    <strong>
                        <i class = "ace-icon fa fa-check"></i>

                    </strong>
                    <?php echo $success; ?>
                </p>
            </div>
            <?php
        }
        ?>
        <form action="check.php" method="post">
            <input type="text" class="form-control" name="email" placeholder="&#xf007; Email"/>
            <input type="password"  class="form-control"  name="password" placeholder="&#xf023; Password" />
            <button type="submit" class="enter form-control">Sign in</button>
        </form>
        <p>Forget your password? <span><a href="email.php">Click here</a></span></p>
    </div>
</div>