<div class="container">
    <div class="col-md-7 col-md-offset-3 login" >
        <h1>SIGN UP</h1>
        <?php
//        session_start();
        require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
        use App\foureach\Required;
        $success = Required::success_message();
        if (isset($success)) {
            ?>
            <div class = "alert alert-success">
                <button type = "button" class = "close" data-dismiss = "alert">
                    <i class = " fa fa-times"></i>
                </button>
                <p>
                    <strong>
                        <i class = "ace-icon fa fa-check"></i>
                        
                    </strong>
                    <?php echo $success; ?>
                </p>
            </div>
            <?php
        }
        ?>
        <form action="sign_up_info.php" method="post">
            <input type="text" class="form-control"  name="first_name" placeholder="&#xf182; First Name" required="required"/>
            <input type="text" class="form-control" name="last_name" placeholder="&#xf182; Last Name" required="required"/>
            <input type="text" class="form-control" name="email" placeholder="&#xf007; Email" required="required"/>
            <input type="password" class="form-control" minlength="8" name="password" placeholder="&#xf023; Password" required="required" />

            <input type="password" class="form-control"  minlength="8" name="retype_password" placeholder="&#xf023; Retype Password" />
            <button type="submit" class="sign_up form-control">Sign Up</button>
        </form>
    </div>
</div>