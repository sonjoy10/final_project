<?php
ini_set('display_errors', 'Off');
include_once 'vendor/autoload.php';
session_start();
use App\foureach\Required;

$unsuccess = Required::success_message();

if (isset($_SESSION['id'])) {
    $user_id = $_SESSION['id'];
    if ($user_id != NULL) {
        Required::redirect();
    }
}
?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Resume for Everybody</title>
        <link rel="stylesheet" href="assets/login/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/login/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/login/style.css" />
    </head>
    <body>
        <header>
            <div class="container">
                <div class="pull-right">
                    <a href="index.php" id="login" class="btn btn-primary">Login</a>
                    <a href="sign_up.php" class="btn btn-success">Sign up</a>
                </div>
            </div>

        </header>
        <section class="center" id="contain">
            <!--  -->
            <?php
            if (isset($pages)) {
                if ($pages == "sign_up_content") {
                    include 'pages/sign_up_content.php';
                } elseif ($pages == "email_content") {
                    include 'pages/email_content.php';
                }
            } else {
                include 'pages/login_content.php';
            }
            ?>
        </section>
        <script type="text/javascript" src="assets/login/js/jquery-2.1.4.min.js"></script>
    </body>
</html>

