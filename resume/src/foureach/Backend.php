<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\foureach;

require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use App\foureach\Required;

class Backend {
    //put your code here
    public function __construct() {
        Required::connection();
    }
    public function index($id = null){
        $data = array();
        $query = "SELECT * FROM `users` WHERE  `id`=".$id;
        $result = mysql_query($query);
//        while ($row = mysql_fetch_object($result)) {
//            $data[] = $row;
//        }
        $row = mysql_fetch_object($result);
        return $row;
    }
    
    public function show($id = null){
        $query = "SELECT * FROM `users` WHERE `id`=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;
    }
    
    public function users_update($data = null){
        $level = $data['user_access'];
        $id = $data['id'];
        $query = "UPDATE `users` SET `is_admin`='".$level."' WHERE `id`=".$id;
        $result = mysql_query($query);
        if($result){
            Required::success_message("Successfully Updated!!");
            Required::redirect2("users_edit.php?id=".$id);
        }else{
            Required::success_message("Not Updated!!");
            Required::redirect2("users_edit.php?id=".$id);
        }
        Required::redirect2("users_edit.php?id=".$id);
    }
    
    public function users_delete($id=null){
        $query = "DELETE FROM `users` WHERE `users`.`id` =".$id;
        $result = mysql_query($query);      
        if ($result) {
            Required::success_message("Your information is successfully Removed!!");
        } else {
            Required::unsuccess_message("Unable to Delete!!");
        }
         Required::redirect2("users.php?id=".$id);
    }
}
