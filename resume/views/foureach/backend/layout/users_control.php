<?php
require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use App\foureach\Backend;
use App\foureach\Required;

$success = Required::success_message();

$obj = new Backend;
$data = $obj->show($_GET['id']);
?>
<section class="content-header">
    <h1>
        Users
        <small>Controls</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Users Access</h3>
            <!--            <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                        </div>-->
        </div>
        <div class="box-body">
            <div class="col-md-8">
                <?php
//                print_r($_SESSION);

                if (isset($success)) {
                    ?>
                    <div class = "alert alert-success">
                        <button type = "button" class = "close" data-dismiss = "alert">
                            <i class = " fa fa-times"></i>
                        </button>
                        <p>
                            <strong>
                                <i class = "ace-icon fa fa-check"></i>

                            </strong>
    <?php echo $success; ?>
                        </p>
                    </div>
                    <?php
                }
                ?>
                <form action="users_update.php" method="post">
                    <div class="dl-horizontal">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                        <dt>Username: </dt>
                        <dd><?php echo $data->user_name; ?></dd>
                        <dt>Email: </dt>
                        <dd><?php echo $data->email; ?></dd>
                        <dt>Users Level: </dt>
                        <dd>
                            <select class="form-control" name="user_access">
                                <option value="<?php echo $data->is_admin; ?>">Level <?php echo $data->is_admin; ?></option>
                                <option value="0">Level 0</option>
                                <option value="1">Level 1</option>
                            </select>
                        </dd>
                        <button type="submit" class="btn btn-success pull-right">Update</button>
                    </div>
                </form>
            </div>
            <!--             /.box-body 
                        <div class="box-footer">
                            Footer
                        </div> /.box-footer-->
        </div><!-- /.box -->

</section><!-- /.content -->
