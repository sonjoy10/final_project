<?php
//    session_start();
ini_set("display_errors","Off");
error_reporting(E_ALL & ~E_DEPRECATED);
////include_once '../../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'resume' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use App\foureach\Frontend;

$obj = new Frontend;
$data = $obj->all_users_info();
?>
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users Table</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>User Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sl = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->user_name; ?></td>
                                    <td><?php echo $value->email; ?></td>
                                    <td>
                                        <?php
                                        if ($value->is_admin == '0') {
                                            echo "Level 0";
                                        } else if ($value->is_admin == '1') {
                                            echo "Level 1";
                                        } else if ($value->is_admin == '2') {
                                            echo "Level 3";
                                        }
                                        ?>

                                    </td>
                                    <td>
                                        <a href="users_edit.php?id=<?php echo $value->id; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="users_delete.php?id=<?php echo $value->id; ?>" id="delete" class="btn btn-success"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $sl++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>User Level</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>